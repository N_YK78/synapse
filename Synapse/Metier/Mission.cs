﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Vues;

namespace Synapse.Metier
{
	class Mission
	{

        private Dictionary<DateTime, int> _releveHorraires;
        private string _nom;
        private string _decimal;
        private decimal _nbHeuresPrevues;

        private Intervenant _executant;

        public Dictionary<DateTime, int> ReleveHorraires
        {
            get
            {
                return _releveHorraires;
            }
        }
    }
}
